clear
clc
close all
%% READ THE IMAGES
file1='P1.PNG';
file2='P2.PNG';
image_A = imread(file1);
% image_A = imresize(image_A,[1702 1276]);
image_B = imread(file2);
% image_B = imresize(image_B,[1702 1276]);
[height_wrap, width_wrap,~] = size(image_A);
[height_unwrap, width_unwrap,~] = size(image_B);

gray_A = im2double(rgb2gray(image_A));
gray_B = im2double(rgb2gray(image_B));
%% FIND HARRIS CORNERS IN BOTH IMAGE
[x_A, y_A, v_A] = harris(gray_A, 1000,'tile',[2 2],'disp');
[x_B, y_B, v_B] = harris(gray_B, 1000,'tile',[2 2],'disp');
ncorners = 500;
[x_A, y_A, ~] = adaptation(x_A, y_A, v_A, ncorners);
[x_B, y_B, ~] = adaptation(x_B, y_B, v_B, ncorners);
%% EXTRACT FEATURE DESCRIPTORS
sigma = 7;
[des_A] = FeatureDescriptor(gray_A, x_A, y_A, sigma);
[des_B] = FeatureDescriptor(gray_B, x_B, y_B, sigma);
%% IMPLEMENT FEATURE MATCHING
dist = dist(des_A,des_B);
[ord_dist, index] = sort(dist, 2);
ratio = ord_dist(:,1)./ord_dist(:,2);
threshold = 0.5;
idx = ratio<threshold;

x_A = x_A(idx);
y_A = y_A(idx);
x_B = x_B(index(idx,1));
y_B = y_B(index(idx,1));

% Show two images and matched points
% imgAB=[image_A,image_B];
% figure,imshow(imgAB);
% showMatchedFeatures(image_A, image_B, [y_A, x_A], [y_B, x_B], 'montage');
%% WARP THE SECOND IMAGE TO THE FIRST IMAGE
npoints = length(x_A);
matcher_A = [y_A, x_A, ones(npoints,1)]';
matcher_B = [y_B, x_B, ones(npoints,1)]';
[hh, ~] = ransac(matcher_B, matcher_A, npoints, 10);
%% USE INVERSE WARP METHOD
[newH, newW, newX, newY, xB, yB] = NewSize(hh, height_wrap, width_wrap, height_unwrap, width_unwrap);

[X,Y] = meshgrid(1:width_wrap,1:height_wrap);
[XX,YY] = meshgrid(newX:newX+newW-1, newY:newY+newH-1);
AA = ones(3,newH*newW);
AA(1,:) = reshape(XX,1,newH*newW);
AA(2,:) = reshape(YY,1,newH*newW);

AA = hh*AA;
XX = reshape(AA(1,:)./AA(3,:), newH, newW);
YY = reshape(AA(2,:)./AA(3,:), newH, newW);
%% WARP IMAGE INTO A NEW IMAGE
newImage(:,:,1) = interp2(X, Y, double(image_A(:,:,1)), XX, YY);
newImage(:,:,2) = interp2(X, Y, double(image_A(:,:,2)), XX, YY);
newImage(:,:,3) = interp2(X, Y, double(image_A(:,:,3)), XX, YY);

newImage = blend(newImage, image_B, xB, yB);
figure,imshow(uint8(newImage));
imwrite(uint8(newImage),'P12.PNG');