function n = dist(x, c)
% DIST2	Calculates squared distance between two sets of points.

[ndata, dimx] = size(x);
[ncentres, dimc] = size(c);
if dimx ~= dimc
    error('Data dimension does not match dimension of centres')
end

a = ones(ncentres, 1);
b = sum((x.^2)', 1);
f = ones(ndata, 1);
d = sum((c.^2)',1);
e = 2.*(x*c');

n = (a*b)' + f*d - e;

% Rounding errors occasionally cause negative entries in n2
if any(any(n<0))
    n(n<0) = 0;
end

end