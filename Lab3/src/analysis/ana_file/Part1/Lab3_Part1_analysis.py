import os
import yaml
import numpy as np
import matplotlib.pyplot as plt


# 3 accelerometers
with open(os.path.join('/Users/yilin/Desktop/lab3_ws/yaml files/imu.yaml')) as f:
    imu = yaml.safe_load_all(f)
    ls = list(imu)
    accelx = []
    accely = []
    accelz = []
    time = []

    for i in range(0, len(ls)-1):
        accelx.append(ls[i]['linear_acceleration']['x'])
        accely.append(ls[i]['linear_acceleration']['y'])
        accelz.append(ls[i]['linear_acceleration']['z'])
        time.append(i/40)

    plt.plot(time, accelx, label="x")
    plt.plot(time, accely, label="y")
    plt.plot(time, accelz, label="z")
    plt.ylabel('Linear Acceleration', fontsize=16)
    plt.xlabel('Time(s)', fontsize=16)
    plt.title('Linear Acceleration in time series', fontsize=20)
    plt.legend()
    plt.show()

# Mean
print("Mean-Accelx: ", np.mean(accelx))
print("Mean-Accely: ", np.mean(accely))
print("Mean-Accelz: ", np.mean(accelz))

plt.plot(time, accelx, label="x", color='orange')
plt.axhline(y=np.mean(accelx), label='x_mean')
plt.ylabel('Linear Acceleration x', fontsize=16)
plt.xlabel('Time(s)', fontsize=16)
plt.title('Linear Acceleration x with mean', fontsize=20)
plt.legend()
plt.show()

plt.plot(time, accely, label="y", color='orange')
plt.axhline(y=np.mean(accely), label='y_mean')
plt.ylabel('Linear Acceleration y', fontsize=16)
plt.xlabel('Time(s)', fontsize=16)
plt.title('Linear Acceleration y with mean', fontsize=20)
plt.legend()
plt.show()

plt.plot(time, accelz, label="z", color='orange')
plt.axhline(y=np.mean(accelz), label='z_mean')
plt.ylabel('Linear Acceleration z', fontsize=16)
plt.xlabel('Time(s)', fontsize=16)
plt.title('Linear Acceleration z with mean', fontsize=20)
plt.legend()
plt.show()

# Standard Deviation
print("SD-Accelx: ", np.std(accelx))
print("SD-Accely: ", np.std(accely))
print("SD-Accelz: ",  np.std(accelz))

plt.plot(time, accelx, label="x", color='orange')
plt.axhline(y=np.std(accelx), label='x_deviation')
plt.ylabel('Linear Acceleration x', fontsize=16)
plt.xlabel('Time(s)', fontsize=16)
plt.title('Linear Acceleration x with standard deviation', fontsize=20)
plt.legend()
plt.show()

plt.plot(time, accely, label="y", color='orange')
plt.axhline(y=np.std(accely), label='y_deviation')
plt.ylabel('Linear Acceleration y', fontsize=16)
plt.xlabel('Time(s)', fontsize=16)
plt.title('Linear Acceleration y with standard deviation', fontsize=20)
plt.legend()
plt.show()

plt.plot(time, accelz, label="z", color='orange')
plt.axhline(y=np.std(accelz), label='z_deviation')
plt.ylabel('Linear Acceleration z', fontsize=16)
plt.xlabel('Time(s)', fontsize=16)
plt.title('Linear Acceleration z with standard deviation', fontsize=20)
plt.legend()
plt.show()

# 3 angular rate gyros
with open(os.path.join('/Users/yilin/Desktop/lab3_ws/yaml files/imu.yaml')) as f:
    imu = yaml.safe_load_all(f)
    ls = list(imu)
    gyrox = []
    gyroy = []
    gyroz = []
    time = []

    for i in range(0, len(ls)-1):
        gyrox.append(float(ls[i]['angular_velocity']['x']))
        gyroy.append(float(ls[i]['angular_velocity']['y']))
        gyroz.append(float(ls[i]['angular_velocity']['z']))
        time.append(i/40)

    plt.scatter(time, gyrox, label='x', marker='+')
    plt.scatter(time, gyroy, label='y', marker='+')
    plt.scatter(time, gyroz, label='z', marker='+')
    plt.ylabel('Angular Rate Gyros', fontsize=16)
    plt.xlabel('Time(s)', fontsize=16)
    plt.title('Angular Rate Gyros in time series', fontsize=20)
    plt.legend()
    plt.show()

# Mean
print("Mean-gyrox: ", np.mean(gyrox))
print("Mean-gyroy: ", np.mean(gyroy))
print("Mean-gyroz: ", np.mean(gyroz))

plt.plot(time, gyrox, label="x", color='orange')
plt.axhline(y=np.mean(gyrox), label='x_mean')
plt.ylabel('Angular Rate Gyro x', fontsize=16)
plt.xlabel('Time(s)', fontsize=16)
plt.title('Angular Rate Gyro x with mean', fontsize=20)
plt.legend()
plt.show()

plt.plot(time, gyroy, label="y", color='orange')
plt.axhline(y=np.mean(gyroy), label='y_mean')
plt.ylabel('Angular Rate Gyro y', fontsize=16)
plt.xlabel('Time(s)', fontsize=16)
plt.title('Angular Rate Gyro y with mean', fontsize=20)
plt.legend()
plt.show()

plt.plot(time, gyroz, label="z", color='orange')
plt.axhline(y=np.mean(gyroz), label='z_mean')
plt.ylabel('Angular Rate Gyro z', fontsize=16)
plt.xlabel('Time(s)', fontsize=16)
plt.title('Angular Rate Gyro z with mean', fontsize=20)
plt.legend()
plt.show()

# Standard Deviation
print("SD-gyrox: ", np.std(gyrox))
print("SD-gyroy: ", np.std(gyroy))
print("SD-gyroz: ", np.std(gyroz))

plt.plot(time, gyrox, label="x", color='orange')
plt.axhline(y=np.std(gyrox), label='x_deviation')
plt.ylabel('Angular Rate Gyro x', fontsize=16)
plt.xlabel('Time(s)', fontsize=16)
plt.title('Angular Rate Gyro x with standard deviation', fontsize=20)
plt.legend()
plt.show()

plt.plot(time, gyroy, label="y", color='orange')
plt.axhline(y=np.std(gyroy), label='y_deviation')
plt.ylabel('Angular Rate Gyro y', fontsize=16)
plt.xlabel('Time(s)', fontsize=16)
plt.title('Angular Rate Gyro y with standard deviation', fontsize=20)
plt.legend()
plt.show()

plt.plot(time, gyroz, label="z", color='orange')
plt.axhline(y=np.std(gyroz), label='z_deviation')
plt.ylabel('Angular Rate Gyro z', fontsize=16)
plt.xlabel('Time(s)', fontsize=16)
plt.title('Angular Rate Gyro z with standard deviation', fontsize=20)
plt.legend()
plt.show()


# 3- axis magnetometers
with open(os.path.join('/Users/yilin/Desktop/lab3_ws/yaml files/mag.yaml')) as f:
    imu = yaml.safe_load_all(f)
    ls = list(imu)
    magx = []
    magy = []
    magz = []
    time = []
    for i in range(0, len(ls)-1):
        magx.append(ls[i]['magnetic_field']['x'])
        magy.append(ls[i]['magnetic_field']['y'])
        magz.append(ls[i]['magnetic_field']['z'])
        time.append(i/40)

    # x_axis = list(range(1, len(magx) + 1))

    plt.plot(time, magx, label="x")
    plt.plot(time, magy, label="y")
    plt.plot(time, magz, label="z")
    plt.ylabel('Magnetometers', fontsize=16)
    plt.xlabel('Time(s)', fontsize=16)
    plt.title('Magnetometers in time series', fontsize=20)
    plt.legend()
    plt.show()

# Mean
print("Mean-magx: ", np.mean(magx))
print("Mean-magy: ", np.mean(magy))
print("Mean-magz: ", np.mean(magz))

plt.plot(time, magx, label="x", color='orange')
plt.axhline(y=np.mean(magx), label='x_mean')
plt.ylabel('Magnetometers x', fontsize=16)
plt.xlabel('Time(s)', fontsize=16)
plt.title('Magnetometers x with mean', fontsize=20)
plt.legend()
plt.show()

plt.plot(time, magy, label="y", color='orange')
plt.axhline(y=np.mean(magy), label='y_mean')
plt.ylabel('Magnetometers y', fontsize=16)
plt.xlabel('Time(s)', fontsize=16)
plt.title('Magnetometers y with mean', fontsize=20)
plt.legend()
plt.show()

plt.plot(time, magz, label="z", color='orange')
plt.axhline(y=np.mean(magz), label='z_mean')
plt.ylabel('Magnetometers z', fontsize=16)
plt.xlabel('Time(s)', fontsize=16)
plt.title('Magnetometers z with mean', fontsize=20)
plt.legend()
plt.show()

# Standard Deviation
print("SD-magx: ", np.std(magx))
print("SD-magy: ", np.std(magy))
print("SD-magz: ", np.std(magz))

plt.plot(time, magx, label="x", color='orange')
plt.axhline(y=np.std(magx), label='x_deviation')
plt.ylabel('Magnetometers x', fontsize=16)
plt.xlabel('Time(s)', fontsize=16)
plt.title('Magnetometers x with standard deviation', fontsize=20)
plt.legend()
plt.show()

plt.plot(time, magy, label="y", color='orange')
plt.axhline(y=np.std(magy), label='y_deviation')
plt.ylabel('Magnetometers y', fontsize=16)
plt.xlabel('Time(s)', fontsize=16)
plt.title('Magnetometers y with standard deviation', fontsize=20)
plt.legend()
plt.show()

plt.plot(time, magz, label="z", color='orange')
plt.axhline(y=np.std(magz), label='z_deviation')
plt.ylabel('Magnetometers z', fontsize=16)
plt.xlabel('Time(s)', fontsize=16)
plt.title('Magnetometers z with standard deviation', fontsize=20)
plt.legend()
plt.show()


# Histogram
plt.subplot(3, 3, 1)
plt.hist(accelx, bins=40, facecolor="blue", edgecolor="black", alpha=0.7)
plt.title('Accelerate - x')
plt.xlabel('Integral')
plt.ylabel('Num')

plt.subplot(3, 3, 2)
plt.hist(accely, bins=40, facecolor="blue", edgecolor="black", alpha=0.7)
plt.title('Accelerate - y')
plt.xlabel('Integral')
plt.ylabel('Num')

plt.subplot(3, 3, 3)
plt.hist(accelz, bins=40, facecolor="blue", edgecolor="black", alpha=0.7)
plt.title('Accelerate - z')
plt.xlabel('Integral')
plt.ylabel('Num')

plt.subplot(3, 3, 4)
plt.hist(gyrox, bins=40, facecolor="blue", edgecolor="black", alpha=0.7)
plt.title('Gyro - x')
plt.xlabel('Integral')
plt.ylabel('Num')

plt.subplot(3, 3, 5)
plt.hist(gyroy, bins=40, facecolor="blue", edgecolor="black", alpha=0.7)
plt.title('Gyro - y')
plt.xlabel('Integral')
plt.ylabel('Num')

plt.subplot(3, 3, 6)
plt.hist(gyroz, bins=40, facecolor="blue", edgecolor="black", alpha=0.7)
plt.title('Gyro - z')
plt.xlabel('Integral')
plt.ylabel('Num')

plt.subplot(3, 3, 7)
plt.hist(magx, bins=40, facecolor="blue", edgecolor="black", alpha=0.7)
plt.title('Mag - x')
plt.xlabel('Integral')
plt.ylabel('Num')

plt.subplot(3, 3, 8)
plt.hist(magy, bins=40, facecolor="blue", edgecolor="black", alpha=0.7)
plt.title('Mag - y')
plt.xlabel('Integral')
plt.ylabel('Num')

plt.subplot(3, 3, 9)
plt.hist(magz, bins=40, facecolor="blue", edgecolor="black", alpha=0.7)
plt.title('Mag - z')
plt.xlabel('Integral')
plt.ylabel('Num')

plt.tight_layout()
plt.show()
