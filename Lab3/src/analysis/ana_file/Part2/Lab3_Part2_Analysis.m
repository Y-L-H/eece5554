clc
clear
close all
%%
bag = rosbag('Part2.bag');
datai = select(bag, 'Topic', '/imu_message');
imu = readMessages(datai,'DataFormat','struct');
%%
Name = {'AngX','AngY','AngZ','AccX','AccY','AccZ'};
Fs = 40;
t0 = 1/Fs;
%%
for i = 1:length(imu)
    oangx(i) = imu{i}.AngularVelocity.X;
    oangy(i) = imu{i}.AngularVelocity.Y;
    oangz(i) = imu{i}.AngularVelocity.Z;
    oaccx(i) = imu{i}.LinearAcceleration.X;
    oaccy(i) = imu{i}.LinearAcceleration.Y;
    oaccz(i) = imu{i}.LinearAcceleration.Z;
end

omega = {oangx',oangy',oangz',oaccx',oaccy',oaccz'};
%%
maxNumM = 100;

for j = 1:6
    theta{j} = cumsum(omega{j}, 1)*t0;
    
    L = size(theta{j}, 1);
    maxM = 2.^floor(log2(L/2));
    m = logspace(log10(1), log10(maxM), maxNumM).';
    m = ceil(m); % m must be an integer.
    m = unique(m); % Remove duplicates.
    
    tau{j} = m*t0;
    
    avar = zeros(numel(m), 1);
    for i = 1:numel(m)
        mi = m(i);
        avar(i,:) = sum((theta{j}(1+2*mi:L) - 2*theta{j}(1+mi:L-mi) + theta{j}(1:L-2*mi)).^2, 1);
    end
    avar = avar ./ (2*tau{j}.^2 .* (L - 2*m));
    
    adev{j} = sqrt(avar);
end

figure
loglog(tau{1},adev{1},tau{2},adev{2},tau{3},adev{3})
title('Allan Deviation(AngularVelocity)')
xlabel('\tau');
ylabel('\sigma(\tau)')
grid on
axis equal
saveas(gcf,['','Allan Deviation(AngularVelocity)','.jpg']);

figure
loglog(tau{4},adev{4},tau{5},adev{5},tau{6},adev{6})
title('Allan Deviation(LinearAcceleration)')
xlabel('\tau');
ylabel('\sigma(\tau)')
grid on
axis equal
saveas(gcf,['','Allan Deviation(LinearAcceleration)','.jpg']);
%% Find the index where the slope of the log-scaled Allan deviation is equal to the slope specified.
for j = 1:6
    slope = -0.5;
    logtau{j} = log10(tau{j});
    logadev{j} = log10(adev{j});
    dlogadev{j} = diff(logadev{j}) ./ diff(logtau{j});
    [~, i] = min(abs(dlogadev{j} - slope));
    
    % Find the y-intercept of the line.
    b(j) = logadev{j}(i) - slope*logtau{j}(i);
    
    % Determine the angle random walk coefficient from the line.
    logN = slope*log(1) + b(j);
    N(j) = 10^logN;
    
    % Plot the results.
    tauN = 1;
    lineN{j} = N(j) ./ sqrt(tau{j});
end
%% Find the index where the slope of the log-scaled Allan deviation is equal to the slope specified.
for j = 1:6
    slope = 0.5;
    [~, i] = min(abs(dlogadev{j} - slope));
    
    % Find the y-intercept of the line.
    b(j) = logadev{j}(i) - slope*logtau{j}(i);
    
    % Determine the rate random walk coefficient from the line.
    logK = slope*log10(3) + b(j);
    K(j) = 10^logK;
    
    % Plot the results.
    tauK = 3;
    lineK{j} = K(j) .* sqrt(tau{j}/3);
end
%% Find the index where the slope of the log-scaled Allan deviation is equal to the slope specified.
for j = 1:6
    slope = 0;
    [~, i] = min(abs(dlogadev{j} - slope));
    
    % Find the y-intercept of the line.
    b(j) = logadev{j}(i) - slope*logtau{j}(i);
    
    % Determine the bias instability coefficient from the line.
    scfB = sqrt(2*log(2)/pi);
    logB(j) = b(j) - log10(scfB);
    B(j) = 10^logB(j);
    
    % Plot the results.
    tauB(j) = tau{j}(i);
    lineB{j} = B(j) * scfB * ones(size(tau{j}));
end
%%
for j = 1:6
    tauParams{j} = [tauN, tauK, tauB(j)];
    params{j} = [N(j), K(j), scfB*B(j)];
    figure
    loglog(tau{j}, adev{j}, tau{j}, [lineN{j}, lineK{j}, lineB{j}], '--', tauParams{j}, params{j}, 'o')
    title('Allan Deviation with Noise Parameters',Name{j})
    xlabel('\tau')
    ylabel('\sigma(\tau)')
    legend('$\sigma (rad/s)$', '$\sigma_N ((rad/s)/\sqrt{Hz})$', ...
        '$\sigma_K ((rad/s)\sqrt{Hz})$', '$\sigma_B (rad/s)$', 'Interpreter', 'latex')
    text(tauParams{j}, params{j}, {'N', 'K', '0.664B'})
    grid on
    axis equal
    saveas(gcf,['','Noise_Parameters',Name{j},'.jpg']);
end
%%
% Simulating the gyroscope measurements takes some time. To avoid this, the
% measurements were generated and saved to a MAT-file. By default, this
% example uses the MAT-file. To generate the measurements instead, change
% this logical variable to true.

for j = 1:6
    gyro = gyroparams('NoiseDensity', N(j), 'RandomWalk', K(j), 'BiasInstability', B(j));
    omegaSim = helperAllanVarianceExample(L, Fs, gyro);
    
    [avarSim{j}, tauSim{j}] = allanvar(omegaSim, 'octave', Fs);
    adevSim{j} = sqrt(avarSim{j});
    adevSim{j} = mean(adevSim{j}, 2); % Use the mean of the simulations.
    
    figure
    loglog(tau{j}, adev{j}, tauSim{j}, adevSim{j}, '--')
    title('Allan Deviation of HW and Simulation',Name{j})
    xlabel('\tau');
    ylabel('\sigma(\tau)')
    legend('HW', 'SIM')
    grid on
    axis equal
    saveas(gcf,['','Simulation',Name{j},'.jpg']);
end