import os
import yaml
import matplotlib.pyplot as plt


# Covered Area Moving
with open(os.path.join('/Users/yilin/Desktop/lab2_ws/CoveredMoving.yaml')) as f:
    cfg = yaml.safe_load_all(f)
    ls = list(cfg)
    utmeast = []
    utmnorth = []
    quality = []
    for i in range(0,len(ls)-1):
        utmeast.append(ls[i]['utm_easting'])
        utmnorth.append(ls[i]['utm_northing'])
        quality.append(ls[i]['quality'])

    plt.plot(utmeast, utmnorth, color='k', linestyle='solid', marker='.', markerfacecolor='r', markeredgecolor='r')
    plt.ylabel('utm_northing', fontsize=16)
    plt.xlabel('utm_easting', fontsize=16)
    plt.title('Covered Area Moving GPS Data', fontsize=20)
    plt.show()

# histogram of quality
    l3 = list(set(quality))
    l3.sort()
    print("Covered Area Moving quality data: ", ",".join(str(i) for i in l3))

    plt.hist(quality, facecolor="b", edgecolor="w", alpha=0.7)
    plt.xlabel('CM GPS quality interval', fontsize=16)
    plt.xticks(l3)
    plt.ylabel('frequency', fontsize=16)
    plt.title('CM GPS quality histogram', fontsize=20)
    plt.show()

# Covered Area Stationary
with open(os.path.join('/Users/yilin/Desktop/lab2_ws/CoveredStationary.yaml')) as f:
    cfg = yaml.safe_load_all(f)
    ls = list(cfg)
    utmeast = []
    utmnorth = []
    quality = []
    for i in range(0,len(ls)-1):
        utmeast.append(ls[i]['utm_easting'])
        utmnorth.append(ls[i]['utm_northing'])
        quality.append(ls[i]['quality'])

    plt.plot(utmeast, utmnorth, color='k', linestyle='solid', marker='.', markerfacecolor='r', markeredgecolor='r')
    #plt.axis([329551.96, 329552.07, 4699254.0, 4699256.0])
    #plt.xlim(329551.96, 329552.07)
    #plt.ylim(4699254.0, 4699256.0)
    plt.ylabel('utm_northing', fontsize=16)
    plt.xlabel('utm_easting', fontsize=16)
    plt.title('Covered Area Stationary GPS Data', fontsize=20)
    plt.show()

# histogram of utm
    l2 = list(set(utmeast))
    l2.sort()
    print("Covered Area Stationary utm_easting data: ", ",".join(str(i) for i in l2))

    plt.hist(utmeast, facecolor="b", edgecolor="w", alpha=0.7)
    plt.xlabel('utm_easting interval', fontsize=16)
    plt.xticks(l2)
    #plt.xticks(np.arange(min(l2), max(l2), 0.008))
    plt.ylabel('frequency', fontsize=16)
    plt.title('UTM_Easting histogram', fontsize=20)
    plt.show()

    l1 = list(set(utmnorth))
    l1.sort()
    print("Covered Area Stationary utm_northing data: ", ",".join(str(i) for i in l1))

    plt.hist(utmnorth, facecolor="b", edgecolor="w", alpha=0.7)
    plt.xlabel('utm_northing interval', fontsize=16)
    plt.ylabel('frequency', fontsize=16)
    plt.title('UTM_Northing histogram', fontsize=20)
    plt.show()

# histogram of quality
    l3 = list(set(quality))
    l3.sort()
    print("Covered Area Stationary quality data: ", ",".join(str(i) for i in l3))

    plt.hist(quality, facecolor="b", edgecolor="w", alpha=0.7)
    plt.xlabel('CS GPS quality interval', fontsize=16)
    plt.xticks(l3)
    plt.ylabel('frequency', fontsize=16)
    plt.title('CS GPS quality histogram', fontsize=20)
    plt.show()

# Open Area Moving
    with open(os.path.join('/Users/yilin/Desktop/lab2_ws/OpenMoving.yaml')) as f:
        cfg = yaml.safe_load_all(f)
        ls = list(cfg)
        utmeast = []
        utmnorth = []
        quality = []
        for i in range(0, len(ls) - 1):
            utmeast.append(ls[i]['utm_easting'])
            utmnorth.append(ls[i]['utm_northing'])
            quality.append(ls[i]['quality'])

        plt.plot(utmeast, utmnorth, color='k', linestyle='solid', marker='.', markerfacecolor='r', markeredgecolor='r')
        plt.ylabel('utm_northing', fontsize=16)
        plt.xlabel('utm_easting', fontsize=16)
        plt.title('Open Area Moving GPS Data', fontsize=20)
        plt.show()

# histogram of quality
    l3 = list(set(quality))
    l3.sort()
    print("Open Area Moving quality data: ", ",".join(str(i) for i in l3))

    plt.hist(quality, facecolor="b", edgecolor="w", alpha=0.7)
    plt.xlabel('OM GPS quality interval', fontsize=16)
    plt.xticks(l3)
    plt.ylabel('frequency', fontsize=16)
    plt.title('OM GPS quality histogram', fontsize=20)
    plt.show()

# Open Area Stationary
    with open(os.path.join('/Users/yilin/Desktop/lab2_ws/OpenStationary.yaml')) as f:
        cfg = yaml.safe_load_all(f)
        ls = list(cfg)
        utmeast = []
        utmnorth = []
        quality = []
        for i in range(0, len(ls) - 1):
            utmeast.append(ls[i]['utm_easting'])
            utmnorth.append(ls[i]['utm_northing'])
            quality.append(ls[i]['quality'])

        plt.plot(utmeast, utmnorth, color='k', linestyle='solid', marker='.', markerfacecolor='r', markeredgecolor='r')
        plt.ylabel('utm_northing', fontsize=16)
        plt.xlabel('utm_easting', fontsize=16)
        plt.title('Open Area Stationary GPS Data', fontsize=20)
        plt.show()

# histogram
    l2 = list(set(utmeast))
    l2.sort()
    print("Open Area Stationary utm_easting data: ", ",".join(str(i) for i in l2))

    plt.hist(utmeast, facecolor="b", edgecolor="w", alpha=0.7)
    plt.xlabel('utm_easting interval', fontsize=16)
    plt.xticks(l2)
    plt.ylabel('frequency', fontsize=16)
    plt.title('UTM_Easting histogram', fontsize=20)
    plt.show()

    l1 = list(set(utmnorth))
    l1.sort()
    print("Open Area Stationary utm_northing data: ", ",".join(str(i) for i in l1))

    plt.hist(utmnorth, facecolor="b", edgecolor="w", alpha=0.7)
    plt.xlabel('utm_northing interval', fontsize=16)
    plt.ylabel('frequency', fontsize=16)
    plt.title('UTM_Northing histogram', fontsize=20)
    plt.show()

# histogram of quality
    l3 = list(set(quality))
    l3.sort()
    print("Open Area Stationary quality data: ", ",".join(str(i) for i in l3))

    plt.hist(quality, facecolor="b", edgecolor="w", alpha=0.7)
    plt.xlabel('OS GPS quality interval', fontsize=16)
    plt.xticks(l3)
    plt.ylabel('frequency', fontsize=16)
    plt.title('OS GPS quality histogram', fontsize=20)
    plt.show()
