# -*- coding: utf-8 -*-
"""
Created on Tue Feb  8 22:10:59 2022

@author: ZMY
"""

import pandas as pd
import matplotlib.pyplot as plt

PointData = pd.read_csv('/Users/yilin/Desktop/lab1_ws/src/gpsdriver/PointData.csv')
MovingData = pd.read_csv('/Users/yilin/Desktop/lab1_ws/src/gpsdriver/MovingData.csv')

x1 = PointData['field.utm_northing']
y1 = PointData['field.utm_easting']
x2 = MovingData['field.utm_northing']
y2 = MovingData['field.utm_easting']

plt.figure(1)
plt.plot(x1,y1, marker='.')
#plt.xlim((4699000,4699500))
#plt.ylim((329000,330000))
plt.show()

plt.figure(2)
plt.plot(x2,y2,marker='.')
plt.show()