#!/usr/bin/env python


import rospy
import serial
import utm
from gps_imu.msg import Gps


if __name__=='__main__':

	gps_data_pub = rospy.Publisher('gps', Gps, queue_size=10)
	gps_data=Gps()
	rospy.init_node("gps_imu", anonymous=True)
	rate = rospy.Rate(1)
	serial_port ='/dev/ttyUSB0'
	serial_baud = 4800
	port = serial.Serial(serial_port,serial_baud,timeout=3.0)
	
	while(not rospy.is_shutdown()):
		line = str(port.readline())
		#print(line[3:8])
		if line[3:8]=='GPGGA':
			data=line[9:-8].split(',')
			print(data)
			if data[5]!='0':
				lat=float(data[1][:2])+float(data[1][2:])/60.0
				#print(lat)
				lat_dir=data[2]
				if lat_dir=='S':
					lat=-lat
				lon=float(data[3][:3])+float(data[3][3:])/60.0
				#print(lon)
				lon_dir=data[4]
				if lon_dir=='W':
					lon=-lon
				status=data[5]
				sat_num=data[6]
				alt=float(data[8])
				#print(alt)
				UTM_coordiante=utm.from_latlon(lat,lon)
	
				gps_data.latitude=lat
				gps_data.longitude=lon
				gps_data.altitude=alt
				gps_data.utm_easting=float(UTM_coordiante[0])
				gps_data.utm_northing=float(UTM_coordiante[1])
				gps_data.zone="%d"%(UTM_coordiante[2])
				gps_data.letter="%s"%(UTM_coordiante[3])
				
				gps_data_pub.publish(gps_data)
				
				rate.sleep()
